struct FetchRatesRequest {
    let accessKey: String
    let baseCurrency: String?
}

extension FetchRatesRequest: Requestable {
    
    var method: RequestMethod { .get }
    var path: String { "/api/latest" }
    
    var query: [String : String]? {
        var query = ["access_key": accessKey]
        query["base"] = baseCurrency
        return query
    }
}
