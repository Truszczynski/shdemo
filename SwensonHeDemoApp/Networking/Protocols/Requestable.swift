import Foundation

enum RequestMethod: String {
    case get, post, put, delete
}

protocol Requestable: Encodable {
    var method: RequestMethod { get }
    var path: String { get }
    var headers: [String: String] { get }
    var query: [String: String]? { get }
}

extension Requestable {
    var headers: [String: String] { [:] }
    var query: [String: String]? { nil }
    func encode(using encoder: JSONEncoder) throws -> Data { try encoder.encode(self) }
}
