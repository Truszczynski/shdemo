import Foundation

class BaseApiWorker {
    
    private let expectedStatusCodes = 200...299
    
    private lazy var jsonEncoder = JSONEncoder()
    private lazy var jsonDecoder = JSONDecoder()
    
    func perform<T: Decodable>(request: Requestable, response: @escaping ((Result<T, ApiError>) -> Void)) {
        
        guard let url = createUrl(from: request) else {
            response(.failure(.invalidUrl))
            return
        }
        
        let urlRequest: URLRequest
            
        do {
            urlRequest = try createUrlRequest(from: url, request: request)
        }
        catch {
            response(.failure(convertToApiError(from: error)))
            return
        }
        
        URLSession.shared.dataTask(with: urlRequest) { [weak self] data, urlResponse, error in
            
            guard let self = self else { return }
            
            do {
                let result: T = try self.handleResponse(with: data, urlResponse: urlResponse, error: error)
                response(.success(result))
            } catch {
                response(.failure(self.convertToApiError(from: error)))
            }
        }
        .resume()
    }
    
    private func createUrl(from request: Requestable) -> URL? {
    
        var urlComponents = URLComponents()
        
        urlComponents.scheme = ApiData.apiScheme
        urlComponents.host = ApiData.apiURL
        urlComponents.path = request.path
        urlComponents.queryItems = request.query?.map { URLQueryItem(name: $0, value: $1) }
        
        return urlComponents.url
    }
    
    private func createUrlRequest(from url: URL, request: Requestable) throws -> URLRequest {
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.allHTTPHeaderFields = request.headers
        urlRequest.httpBody = try createBody(using: request)
        
        return urlRequest
    }
    
    private func createBody(using request: Requestable) throws -> Data? {
        switch request.method {
        case .get, .delete:
            return nil
        case .post, .put:
            do { return try request.encode(using: jsonEncoder) }
            catch { throw ApiError.unableToEncode(error: error) }
        }
    }
    
    private func handleResponse<T: Decodable>(with data: Data?, urlResponse: URLResponse?, error: Error?) throws -> T {
        
        guard let data = data, let httpResponse = urlResponse as? HTTPURLResponse else { throw ApiError.invalidResponse(error: error) }
        guard expectedStatusCodes.contains(httpResponse.statusCode) else { throw ApiError.unexpectedHttpCode(code: httpResponse.statusCode) }
        
        do { return try jsonDecoder.decode(T.self, from: data) }
        catch { throw decodeError(from: data) }
    }
    
    private func convertToApiError(from error: Error) -> ApiError {
        guard let apiError = error as? ApiError else { return .unhandledError }
        return apiError
    }
    
    private func decodeError(from data: Data) -> ApiError {
        do {
            let errorModel = try jsonDecoder.decode(ApiErrorModel.self, from: data)
            return .apiError(error: errorModel)
        } catch {
            return .unableToDecode(error: error)
        }
    }
}
