struct ApiErrorModel: Decodable {
    
    struct Error: Decodable {
        let code: Int
        let type: String
    }
    
    let success: Bool
    let error: Error
}
