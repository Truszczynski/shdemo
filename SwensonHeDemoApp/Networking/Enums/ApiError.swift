enum ApiError: Error {
    case invalidUrl
    case invalidResponse(error: Error?)
    case unexpectedHttpCode(code: Int)
    case unableToEncode(error: Error)
    case unableToDecode(error: Error)
    case apiError(error: ApiErrorModel)
    case unhandledError
}
