import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder {
    
    var window: UIWindow?
    
    private func setupAppearances() {
        UINavigationBar.appearance().barTintColor = .blue
        UINavigationBar.appearance().tintColor = .white
    }
    
    private func prepareFirstScene() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = CurrencySelectorSceneConfigurator.makeScene()
        window?.makeKeyAndVisible()
    }
}

extension AppDelegate: UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupAppearances()
        prepareFirstScene()
        return true
    }
}
