protocol CurrencyConverterInteractable {
    func requestConversion(from value: Double)
}

final class CurrencyConverterInteractor {
    var presenter: CurrencyConverterPresentable?
}

extension CurrencyConverterInteractor: CurrencyConverterInteractable {
    
    func requestConversion(from value: Double) {
        presenter?.conversionRequestReceived(value)
    }
}
