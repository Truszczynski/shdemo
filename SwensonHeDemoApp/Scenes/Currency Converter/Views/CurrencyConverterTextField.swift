import UIKit

final class CurrencyConverterTextField: UIView {
    
    // MARK: - Subviews
    
    private let valueTextField: UITextField = {
        let view = UITextField()
        view.font = .systemFont(ofSize: 24.0)
        view.keyboardType = .decimalPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let currencyLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Properties
    
    var onValueChange: ((String?) -> Void)?
    
    // MARK: - Constructors
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setups
    
    private func setup() {
        setupViews()
        setupContraints()
        setupFeedbacks()
    }
    
    private func setupViews() {
        backgroundColor = .white
        [valueTextField, currencyLabel].forEach { addSubview($0) }
    }
    
    private func setupContraints() {
        
        valueTextField.setContentHuggingPriority(.defaultLow, for: .horizontal)
        currencyLabel.setContentHuggingPriority(.required, for: .horizontal)
        currencyLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        
        let constraints = [
            valueTextField.topAnchor.constraint(equalTo: topAnchor, constant: 50.0),
            valueTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 50.0),
            valueTextField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -50.0),
            currencyLabel.topAnchor.constraint(equalTo: topAnchor, constant: 50.0),
            currencyLabel.leadingAnchor.constraint(equalTo: valueTextField.trailingAnchor, constant: 16.0),
            currencyLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -50.0),
            currencyLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -50.0)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setupFeedbacks() {
        valueTextField.addTarget(self, action: #selector(valueWasChanged(_:)), for: .editingChanged)
    }
    
    // MARK: - Updates
    
    func update(value: String?) {
        valueTextField.text = value
    }
    
    func upadate(currency: String?) {
        currencyLabel.text = currency
    }
    
    // MARK: - Target Actions
    
    @objc private func valueWasChanged(_ textField: UITextField) {
        onValueChange?(textField.text)
    }
    
    // MARK - Overrides
    
    override func becomeFirstResponder() -> Bool { valueTextField.becomeFirstResponder() }
}
