import UIKit

final class CurrencyConverterView: UIView {
    
    // MARK: - Components
    
    struct ViewModel {
        var baseCurrency: String
        var targetCurrency: String
    }
    
    // MARK: - Subviews
    
    private let baseCurrencyTextField: CurrencyConverterTextField = {
        let view = CurrencyConverterTextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let targetCurrencyTextField: CurrencyConverterTextField = {
        let view = CurrencyConverterTextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Properties
    
    var onValueChange: ((String?) -> Void)? {
        get { baseCurrencyTextField.onValueChange }
        set { baseCurrencyTextField.onValueChange = newValue }
    }
    
    // MARK: - Constructors
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setups
    
    private func setup() {
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        backgroundColor = .blue
        [baseCurrencyTextField, targetCurrencyTextField].forEach { addSubview($0) }
        targetCurrencyTextField.isUserInteractionEnabled = false
    }
    
    private func setupConstraints() {
        
        let constraints = [
            baseCurrencyTextField.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            baseCurrencyTextField.leadingAnchor.constraint(equalTo: leadingAnchor),
            baseCurrencyTextField.trailingAnchor.constraint(equalTo: trailingAnchor),
            targetCurrencyTextField.topAnchor.constraint(equalTo: baseCurrencyTextField.bottomAnchor),
            targetCurrencyTextField.leadingAnchor.constraint(equalTo: leadingAnchor),
            targetCurrencyTextField.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    // MARK: - Updates
    
    func update(with viewModel: ViewModel) {
        baseCurrencyTextField.upadate(currency: viewModel.baseCurrency)
        targetCurrencyTextField.upadate(currency: viewModel.targetCurrency)
    }
    
    func update(baseValue: String) {
        baseCurrencyTextField.update(value: baseValue)
    }
    
    func update(targetValue: String) {
        targetCurrencyTextField.update(value: targetValue)
    }
    
    // MARK: - Actions
    
    func focusOnTextField() {
        _ = baseCurrencyTextField.becomeFirstResponder()
    }
    
    func removeFocus() {
        endEditing(true)
    }
}
