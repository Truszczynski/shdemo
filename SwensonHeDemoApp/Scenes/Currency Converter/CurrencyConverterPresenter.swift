protocol CurrencyConverterPresentable {
    func conversionRequestReceived(_ value: Double)
}

final class CurrencyConverterPresenter {
    
    // MARK: - Properties
    
    weak var controller: CurrencyConverterViewControllable?
    
    private let currencyValueParser: CurrencyValueParserable
    private let exchangeRate: Double
    
    // MARK: - Constructors
    
    init(currencyValueParser: CurrencyValueParserable, exchangeRate: Double) {
        self.currencyValueParser = currencyValueParser
        self.exchangeRate = exchangeRate
    }
}

extension CurrencyConverterPresenter: CurrencyConverterPresentable {
    
    func conversionRequestReceived(_ value: Double) {
        let targetValue = currencyValueParser.string(from: value * exchangeRate)
        controller?.update(with: targetValue)
    }
}
