import UIKit

protocol CurrencyConverterViewControllable: class {
    func update(with exchangeValue: String)
    func showConversionError()
}

final class CurrencyConverterViewController: UIViewController {
    
    // MARK: - Properties
    
    var interactor: CurrencyConverterInteractable?
    private let mainView = CurrencyConverterView()
    
    // MARK: - Constructors
    
    init(baseCurrency: String, targetCurrency: String) {
        super.init(nibName: nil, bundle: nil)
        mainView.update(with: CurrencyConverterView.ViewModel(baseCurrency: baseCurrency, targetCurrency: targetCurrency))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecycle
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mainView.focusOnTextField()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainView.removeFocus()
    }
    
    // MARK: - Setups
    
    private func setup() {
        setupData()
        setupFeedbacks()
    }
    
    private func setupData() {
        interactor?.requestConversion(from: 1)
        mainView.update(baseValue: "1")
    }
    
    private func setupFeedbacks() {
        
        mainView.onValueChange = { [weak self] value in
            guard let value = value, let rawValue = Double(value) else {
                self?.showErrorMessage()
                return
            }
            self?.interactor?.requestConversion(from: rawValue)
        }
    }
    
    // MARK: - Actions
    
    private func showErrorMessage() {
        mainView.update(targetValue: "general.error".localized)
    }
}

extension CurrencyConverterViewController: CurrencyConverterViewControllable {
    
    func update(with exchangeValue: String) {
        mainView.update(targetValue: exchangeValue)
    }
    
    func showConversionError() {
        showErrorMessage()
    }
}
