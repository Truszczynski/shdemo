import UIKit

final class CurrencyConverterSceneConfigurator {
    
    static func makeScene(baseCurrency: String, targetCurrency: String, exchangeRate: Double) -> UIViewController {
        
        let controller = CurrencyConverterViewController(baseCurrency: baseCurrency, targetCurrency: targetCurrency)
        let interactor = CurrencyConverterInteractor()
        let presenter = CurrencyConverterPresenter(currencyValueParser: CurrencyValueParser(), exchangeRate: exchangeRate)
        
        controller.interactor = interactor
        interactor.presenter = presenter
        presenter.controller = controller
        
        return controller
    }
}
