import UIKit

final class RateCell: UITableViewCell {
    
    // MARK: - Components
    
    struct ViewModel {
        let title: String
        let value: String
    }
    
    // MARK: - Constructors
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setups
    
    private func setup() {
        accessoryType = .disclosureIndicator
    }
    
    // MARK: - Updates
    
    func update(with viewModel: ViewModel) {
        textLabel?.text = viewModel.title
        detailTextLabel?.text = viewModel.value
    }
}
