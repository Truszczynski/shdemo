import UIKit

final class CurrencySelectorView: UIView {
    
    // MARK: - Subviews
    
    private let tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(cell: RateCell.self)
        return view
    }()
    
    // MARK: - Properties
    
    private var rowsViewModels: [RateCell.ViewModel] = []
    
    var onRowSelect: ((Int) -> Void)?
    
    // MARK: - Constructors
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setups
    
    private func setup() {
        setupViews()
        setupContraints()
        setupFeedbacks()
    }
    
    private func setupViews() {
        backgroundColor = .white
        addSubview(tableView)
    }
    
    private func setupContraints() {
        
        let constraints = [
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setupFeedbacks() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK: - Updates
    
    func update(with rowsViewModels: [RateCell.ViewModel]) {
        self.rowsViewModels = rowsViewModels
        tableView.reloadData()
    }
}

extension CurrencySelectorView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int { 1 }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { rowsViewModels.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue(reusableCell: RateCell.self, for: indexPath)
        let rowModel = rowsViewModels[indexPath.row]
        
        cell.update(with: rowModel)
        
        return cell
    }
}

extension CurrencySelectorView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onRowSelect?(indexPath.row)
    }
}
