import UIKit

final class CurrencySelectorButton: UIButton {
    
    // MARK: - Properties
    
    var onTap: (() -> Void)?
    
    // MARK: - Constructors
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setups
    
    private func setup() {
        setupView()
        setupConstraints()
        setupFeedbacks()
    }
    
    private func setupView() {
        backgroundColor = .white
        setTitleColor(.black, for: .normal)
        setTitleColor(.darkGray, for: .highlighted)
    }
    
    private func setupConstraints() {
        widthAnchor.constraint(equalToConstant: 120.0).isActive = true
    }
    
    private func setupFeedbacks() {
        addTarget(self, action: #selector(buttonWasPressed(_:)), for: .touchUpInside)
    }
    
    // MARK: - Updates
    
    func update(with title: String) {
        setTitle(title, for: .normal)
    }
    
    // MARK: - Target Actions
    
    @objc private func buttonWasPressed(_ button: CurrencySelectorButton) {
        onTap?()
    }
}
