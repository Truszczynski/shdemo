import UIKit

struct CurrencySelectorViewModel {
    let baseCurrency: String
    let rows: [RateCell.ViewModel]
}

protocol CurrencySelectorViewControllable: class {
    func update(with viewModel: CurrencySelectorViewModel)
    func showCurrencySelector(with currencies: [String])
    func moveToExchangeScene(with exchangeData: ExchangeRateData)
    func showError(with message: String)
}

final class CurrencySelectorViewController: UIViewController, OverlayShowable {
    
    // MARK: - Properties
    
    var interactor: CurrencySelectorInteractable?
    
    private let mainView = CurrencySelectorView()
    private let baseCurrencyButton = CurrencySelectorButton()
    private let router: CurrencySelectorRoutable
    
    // MARK: - Constructors
    
    init(router: CurrencySelectorRoutable) {
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecycle
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchRates(with: nil)
        setup()
    }
    
    // MARK: - Setups
    
    private func setup() {
        setupFeedbacks()
        setupNavigationItem()
    }
    
    private func setupFeedbacks() {
        
        baseCurrencyButton.onTap = { [weak self] in
            self?.fetchCurrencies()
        }
        
        mainView.onRowSelect = { [weak self] row in
            self?.fetchExchangeData(for: row)
        }
    }
    
    private func setupNavigationItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshButtonWasPressed(_:)))
    }
    
    // MARK: - Actions
    
    private func fetchRates(with baseCurrency: String?) {
        show(overlay: SpinnerController())
        interactor?.fetchRates(with: baseCurrency)
    }
    
    private func fetchCurrencies() {
        interactor?.fetchCurrencies()
    }
    
    private func fetchExchangeData(for row: Int) {
        interactor?.fetchExchangeData(for: row)
    }
    
    private func updateBaseCurrencyButton(with title: String) {
        baseCurrencyButton.setTitle(title, for: .normal)
        
        navigationItem.titleView =  nil
        navigationItem.titleView = baseCurrencyButton
    }
    
    private func prepareCurrencySelector(with currencies: [String]) {
        
        let controller = UIAlertController(title: "currencySelectorScene.baseCurrencyButton.text".localized, message: nil, preferredStyle: .actionSheet)
        
        currencies
            .map { makeAction(for: $0) }
            .forEach { controller.addAction($0) }
        
        present(controller, animated: true)
    }
    
    private func makeAction(for currency: String) -> UIAlertAction {
        
        UIAlertAction(title: currency, style: .default) { [weak self] _ in
            self?.fetchRates(with: currency)
        }
    }
    
    private func prepareAlert(with message: String) {
        
        let controller = UIAlertController(title: "general.error".localized, message: message, preferredStyle: .alert)
        
        controller.addAction(UIAlertAction(title: "general.ok".localized, style: .default, handler: nil))
        present(controller, animated: true)
    }
    
    // MARK: - Target Actions
    
    @objc private func refreshButtonWasPressed(_ button: UIBarButtonItem) {
        fetchRates(with: nil)
    }
}

extension CurrencySelectorViewController: CurrencySelectorViewControllable {
    
    func update(with viewModel: CurrencySelectorViewModel) {
        hideOverlay()
        updateBaseCurrencyButton(with: viewModel.baseCurrency)
        mainView.update(with: viewModel.rows)
    }
    
    func showCurrencySelector(with currencies: [String]) {
        prepareCurrencySelector(with: currencies)
    }
    
    func moveToExchangeScene(with exchangeData: ExchangeRateData) {
        router.moveToCurrencyConverterScene(with: exchangeData)
    }
    
    func showError(with message: String) {
        hideOverlay(completion: { [weak self] in
            self?.prepareAlert(with: message)
        })
    }
}
