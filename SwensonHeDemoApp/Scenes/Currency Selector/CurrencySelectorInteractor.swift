enum CurrencySelectorError: Error {
    case noChachedData
}

protocol CurrencySelectorInteractable {
    func fetchRates(with baseCurrency: String?)
    func fetchCurrencies()
    func fetchExchangeData(for row: Int)
}

final class CurrencySelectorInteractor {
    
    // MARK: - Properties
    
    var presenter: CurrencySelectorPresentable?
    private let latestRatesWorker: LatestRatesApiWorkable
    
    // MARK: - Constructors
    
    init(latestRatesWorker: LatestRatesApiWorkable) {
        self.latestRatesWorker = latestRatesWorker
    }
}

extension CurrencySelectorInteractor: CurrencySelectorInteractable {
    
    func fetchRates(with baseCurrency: String?) {
        
        let request = FetchRatesRequest(accessKey: ApiData.accessKey, baseCurrency: baseCurrency)
        
        latestRatesWorker.fetchLatestData(with: request) { [weak self] result in
            
            switch result {
            case let .success(model):
                self?.presenter?.ratesReceived(model)
            case let .failure(error):
                self?.presenter?.apiErrorReceived(error)
            }
        }
    }
    
    func fetchCurrencies() {
        presenter?.currenciesRequestReceived()
    }
    
    func fetchExchangeData(for row: Int) {
        presenter?.exchangeRequestReceived(row)
    }
}
