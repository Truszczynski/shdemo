import UIKit

struct ExchangeRateData {
    let baseCurrency: String
    let targetCurrency: String
    let exchangeRate: Double
}

protocol CurrencySelectorRoutable {
    func moveToCurrencyConverterScene(with inputData: ExchangeRateData)
}

final class CurrencySelectorRouter {
    weak var navigator: UINavigationController?
}

extension CurrencySelectorRouter: CurrencySelectorRoutable {
    
    func moveToCurrencyConverterScene(with inputData: ExchangeRateData) {
        let controller = CurrencyConverterSceneConfigurator.makeScene(baseCurrency: inputData.baseCurrency, targetCurrency: inputData.targetCurrency, exchangeRate: inputData.exchangeRate)
        navigator?.pushViewController(controller, animated: true)
    }
}
