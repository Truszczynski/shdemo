import UIKit

final class CurrencySelectorSceneConfigurator {
    
    static func makeScene() -> UIViewController {
        
        let router = CurrencySelectorRouter()
        let controller = CurrencySelectorViewController(router: router)
        let interactor = CurrencySelectorInteractor(latestRatesWorker: LatestRatesApiWorker())
        let presenter = CurrencySelectorPresenter(currencyValueParser: CurrencyValueParser(), errorTranslator: ErrorTranslator())
        let navigator = UINavigationController(rootViewController: controller)
        
        router.navigator = navigator
        
        controller.interactor = interactor
        interactor.presenter = presenter
        presenter.controller = controller
        
        return navigator
    }
}
