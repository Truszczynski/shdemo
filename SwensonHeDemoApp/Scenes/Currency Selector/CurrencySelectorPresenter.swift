import Foundation

protocol CurrencySelectorPresentable {
    func ratesReceived(_ model: LatestRates)
    func currenciesRequestReceived()
    func exchangeRequestReceived(_ row: Int)
    func apiErrorReceived(_ error: ApiError)
}

final class CurrencySelectorPresenter {
    
    // MARK: - Properties
    
    weak var controller: CurrencySelectorViewControllable?
    
    private let currencyValueParser: CurrencyValueParserable
    private let errorTranslator: ErrorTranslatable
    
    private var cachedBaseCurrency: String?
    private var cachedRates: [(key: String, value: Double)]?
    
    // MARK: - Constructors
    
    init(currencyValueParser: CurrencyValueParserable, errorTranslator: ErrorTranslatable) {
        self.currencyValueParser = currencyValueParser
        self.errorTranslator = errorTranslator
    }
    
    // MARK: - Actions
    
    private func showGenericError() {
        controller?.showError(with: "general.error".localized)
    }
}

extension CurrencySelectorPresenter: CurrencySelectorPresentable {
    
    func ratesReceived(_ model: LatestRates) {
        
        let rates = model.rates.sorted { $0.key < $1.key }
        let rows = rates.map { RateCell.ViewModel(title: $0, value: currencyValueParser.string(from:$1)) }
        let viewModel = CurrencySelectorViewModel(baseCurrency: model.base, rows: rows)
        
        cachedBaseCurrency = model.base
        cachedRates = rates
        
        DispatchQueue.main.async { [weak self] in
            self?.controller?.update(with: viewModel)
        }
    }
    
    func currenciesRequestReceived() {
        
        guard let cachedRates = cachedRates else {
            showGenericError()
            return
        }
        
        let currencies = cachedRates.map { $0.key }
        controller?.showCurrencySelector(with: currencies)
    }
    
    func exchangeRequestReceived(_ row: Int) {
        
        guard let cachedBaseCurrency = cachedBaseCurrency, let cachedRates = cachedRates else {
            showGenericError()
            return
        }
        
        let targetRate = cachedRates[row]
        let exchangeRateData = ExchangeRateData(baseCurrency: cachedBaseCurrency, targetCurrency: targetRate.key, exchangeRate: targetRate.value)
        
        controller?.moveToExchangeScene(with: exchangeRateData)
    }
    
    func apiErrorReceived(_ error: ApiError) {
        
        switch error {
        case let .apiError(error):
            let message = errorTranslator.translate(error: error)
            controller?.showError(with: message)
        case .invalidResponse, .invalidUrl, .unableToDecode, .unableToEncode, .unexpectedHttpCode, .unhandledError:
            controller?.showError(with: "general.error".localized)
        }
    }
}
