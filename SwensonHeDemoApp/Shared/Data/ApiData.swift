struct ApiData {
    static let apiScheme: String = "http"
    static let apiURL: String = "data.fixer.io"
    static let accessKey: String = "<Your Access Key>"
}
