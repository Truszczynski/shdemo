import UIKit

final class SpinnerController: UIViewController {
    
    // MARK: - Constructors
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Flow
    
    override func loadView() {
        view = SpinnerView()
    }
}
