import UIKit

final class SpinnerView: UIView {
    
    // MARK: - Subviews
    
    private let indicatorView: IndicatorView = {
        let view = IndicatorView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Constructors
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setups
    
    private func setup() {
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        addSubview(indicatorView)
    }
    
    private func setupConstraints() {
        
        let constraints = [
            indicatorView.topAnchor.constraint(equalTo: topAnchor, constant: 28.0),
            indicatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -28.0)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
