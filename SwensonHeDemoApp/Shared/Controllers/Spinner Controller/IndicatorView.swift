import UIKit

final class IndicatorView: UIView {
    
    private let activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.color = .white
        view.startAnimating()
        return view
    }()
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        layer.cornerRadius = 6.0
        addSubview(activityIndicator)
    }
    
    private func setupConstraints() {
        
        let constraints = [
            activityIndicator.topAnchor.constraint(equalTo: topAnchor, constant: 12.0),
            activityIndicator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            activityIndicator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
            activityIndicator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12.0)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
