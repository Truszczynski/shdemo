protocol ErrorTranslatable {
    func translate(error: ApiErrorModel) -> String
}

final class ErrorTranslator {
}

extension ErrorTranslator: ErrorTranslatable {
    func translate(error: ApiErrorModel) -> String { "error.api.\(error.error.code)".localized }
}
