protocol CurrencyValueParserable {
    func string(from: Double) -> String
}

final class CurrencyValueParser {
}

extension CurrencyValueParser: CurrencyValueParserable {
    func string(from value: Double) -> String { String(format: "%0.2f", value) }
}
