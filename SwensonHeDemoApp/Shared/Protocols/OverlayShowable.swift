import UIKit

protocol OverlayShowable {
}

extension OverlayShowable where Self: UIViewController {
    
    func show(overlay: UIViewController) {
        guard presentedViewController == nil else { return }
        
        DispatchQueue.main.async {
            self.present(overlay, animated: true)
        }
    }
    
    func hideOverlay(completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            guard let overlay = self.presentedViewController else { return }
            overlay.dismiss(animated: true, completion: completion)
        }
    }
}
