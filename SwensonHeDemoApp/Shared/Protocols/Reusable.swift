import UIKit

protocol Reusable: class {}

extension Reusable {
    static var identifier: String { String(describing: self) }
}

extension UITableViewCell: Reusable {}


extension UITableView {
    
    func register(cell: Reusable.Type) {
        register(cell, forCellReuseIdentifier: cell.identifier)
    }
    
    func dequeue<T: UITableViewCell>(reusableCell: T.Type, for indexPath: IndexPath) -> T { dequeueReusableCell(withIdentifier: reusableCell.identifier, for: indexPath) as? T ?? T(style: .default, reuseIdentifier: T.identifier) }
}
