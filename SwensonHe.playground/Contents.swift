import Foundation

// MARK: - Helpers

extension Bool {
    
    func printResult() {
        self ? print("😊") : print("😢")
    }
}

// ----- 1 -----

let result: Double = (3+1)/3*9
(result == 12.0).printResult()

// ----- 2 -----

extension String {
    func isAnagram(to text: String) -> Bool { unified == text.unified }
    private var unified: [Character] { filter { String($0) != " " }.lowercased().sorted() }
}

"debit card".isAnagram(to: "bad credit").printResult()
"punishment".isAnagram(to: "nine thumps").printResult()

// ----- 3 -----

func recursiveFibonacci(targetElement: Int) -> Int {
    guard targetElement > 1 else { return targetElement }
    return recursiveFibonacci(targetElement: targetElement - 1) + recursiveFibonacci(targetElement: targetElement - 2)
}

func iterativeFibonacci(targetElement: Int) -> Int {
    guard targetElement > 1 else { return targetElement }
    var firstValue = 0
    var secondValue = 1
    var index = 1
    
    while targetElement != index {
        let newResult = firstValue + secondValue
        firstValue = secondValue
        secondValue = newResult
        index += 1
    }
    
    return secondValue
    
}

(recursiveFibonacci(targetElement: 5) == 5).printResult()
(recursiveFibonacci(targetElement: 8) == 21).printResult()

(iterativeFibonacci(targetElement: 5) == 5).printResult()
(iterativeFibonacci(targetElement: 8) == 21).printResult()
